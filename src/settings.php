<?php
return [
    'settings' => [
        'doctrine' => [
            'meta' => [
                'entity_path' => [
                    __DIR__.'/App/Entity',
                ],
                'auto_generate_proxies' => true,
                'proxy_dir' => __DIR__.'/../cache/proxies',
                'cache' => null,
            ],
            'connection' => [
                'driver' => 'pdo_mysql',
                'host' => 'localhost',
                'dbname' => 'test',
                'user' => 'walid',
                'password' => '',
            ],
        ],
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__.'/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__.'/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        //        'pdo' => [
//            'dsn' => 'mysql:dbname=middlware;host=127.0.0.1',
//            'login' => 'middlware',
//            'pwd' => ''
//        ],
    ],
];
