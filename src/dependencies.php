<?php
// DIC configuration

use App\Controller\FieldController;
use App\Controller\FormController;
use App\Controller\InputController;
use App\Controller\SavingController;
use App\Controller\UserController;
use App\Resource\FieldResource;
use App\Resource\FormResource;
use App\Resource\InputResource;
use App\Resource\SavingResource;
use App\Resource\UserResource;
use Doctrine\ORM\EntityManager;

require __DIR__.'/../vendor/autoload.php';

$container = $app->getContainer();

$container['UserController'] = function ($c) {
    $userResource = new UserResource($c->get('em'));
    return new UserController($userResource);
};

$container['FormController'] = function ($c) {
    $userResource = new FormResource($c->get('em'));
    return new FormController($userResource);
};

$container['FieldController'] = function ($c) {
    $FieldResource = new FieldResource($c->get('em'));
    return new FieldController($FieldResource);
};

$container['InputController'] = function ($c) {
    $FieldResource = new InputResource($c->get('em'));
    return new InputController($FieldResource);
};

$container['SavingController'] = function ($c) {
    $savingResource = new SavingResource($c->get('em'));
    return new SavingController($savingResource);
};

$container['em'] = function ($c) {
    $settings = $c->get('settings');
    $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
        $settings['doctrine']['meta']['entity_path'],
        $settings['doctrine']['meta']['auto_generate_proxies'],
        $settings['doctrine']['meta']['proxy_dir'],
        $settings['doctrine']['meta']['cache'],
        false
    );

    return EntityManager::create($settings['doctrine']['connection'], $config);
};

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];

    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    return $logger;
};

