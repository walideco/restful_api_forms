<?php
// Routes
use App\Middleware\InputsMiddleware;
use App\Middleware\ModelsMiddleware;
use App\Middleware\SavingsMiddleware;
use App\Middleware\UserAdminMiddleware;

$userAdminMiddleware = new UserAdminMiddleware($app->getContainer()['em']);
$modelsMiddleware = new ModelsMiddleware($app->getContainer()['em']);
$savingsMiddleware = new SavingsMiddleware($app->getContainer()['em']);
$inputsMiddleware = new InputsMiddleware($app->getContainer()['em']);
$baseUrl = '/api/v1';

$app->get($baseUrl.'/users', 'UserController:getUsers')
    ->setName('users')
    ->add($userAdminMiddleware);
$app->get($baseUrl.'/users/{userId:\d+}', 'UserController:getUser')
    ->setName('user')
    ->add($userAdminMiddleware);
$app->post($baseUrl.'/users', 'UserController:create')
    ->setName('user-create')
    ->add($userAdminMiddleware);
$app->patch($baseUrl.'/users/{userId:\d+}', 'UserController:update')
    ->setName('user-update')
    ->add($userAdminMiddleware);
$app->delete($baseUrl.'/users/{userId:\d+}', 'UserController:delete')
    ->setName('user-delete')
    ->add($userAdminMiddleware);

$app->get($baseUrl.'/forms', 'FormController:getForms')
    ->setName('forms')
    ->add($modelsMiddleware);
$app->get($baseUrl.'/forms/{formId:\d+}', 'FormController:getForm')
    ->setName('form')
    ->add($modelsMiddleware);
$app->post($baseUrl.'/forms', 'FormController:create')
    ->setName('form-create')
    ->add($modelsMiddleware);
$app->patch($baseUrl.'/forms/{formId:\d+}', 'FormController:update')
    ->setName('form-update')
    ->add($modelsMiddleware);
$app->delete($baseUrl.'/forms/{formId:\d+}', 'FormController::delete')
    ->setName('form-delete')
    ->add($modelsMiddleware);

$app->get($baseUrl.'/forms/{formId:\d+}/fields', 'FieldController:getFields')
    ->setName('fields')
    ->add($modelsMiddleware);
$app->get($baseUrl.'/forms/{formId:\d+}/fields/{fieldId:\d+}', 'FieldController:getField')
    ->setName('field')
    ->add($modelsMiddleware);
$app->post($baseUrl.'/forms/{formId:\d+}/fields', 'FieldController:create')
    ->setName('field-create')
    ->add($modelsMiddleware);
$app->patch($baseUrl.'/forms/{formId:\d+}/fields/{fieldId:\d+}', 'FieldController:update')
    ->setName('field-update')
    ->add($modelsMiddleware);
$app->delete($baseUrl.'/forms/{formId:\d+}/fields/{fieldId:\d+}', 'FieldController:delete')
    ->setName('field-delete')
    ->add($modelsMiddleware);

$app->get($baseUrl.'/forms/{formId:\d+}/savings', 'SavingController:getSavings')
    ->setName('savings')
    ->add($savingsMiddleware);
$app->get($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}', 'SavingController:getSaving')
    ->setName('saving')
    ->add($savingsMiddleware);
$app->post($baseUrl.'/forms/{formId:\d+}/savings', 'SavingController:create')
    ->setName('saving-create')
    ->add($savingsMiddleware);
$app->patch($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}', 'SavingController:update')
    ->setName('saving-update')
    ->add($savingsMiddleware);
$app->delete($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}', 'SavingController:delete')
    ->setName('saving-delete')
    ->add($userAdminMiddleware);
$app->patch($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}/valid', 'SavingController:validate')
    ->setName('saving-validate');


$app->get($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}/inputs', 'InputController:getInputs')
    ->setName('inputs')
    ->add($inputsMiddleware);
$app->get($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}/inputs/{inputId:\d+}', 'InputController:getInput')
    ->setName('input')
    ->add($inputsMiddleware);
$app->post($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}/inputs', 'InputController:create')
    ->setName('input-create')
    ->add($inputsMiddleware);
$app->patch($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\d+}/inputs/{inputId:\d+}', 'InputController:update')
    ->setName('input-update')
    ->add($inputsMiddleware);
$app->delete($baseUrl.'/forms/{formId:\d+}/savings/{savingId:\sd+}/inputs/{inputId:\d+}', 'InputController:delete')
    ->setName('input-delete')
    ->add($inputsMiddleware);

//TODO : Add routes for FieldType Resource

