<?php

namespace App;


use Doctrine\ORM\EntityManager;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class AbstractMiddleware
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager = null;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public abstract function __invoke(Request $request, Response $response, $next);
}