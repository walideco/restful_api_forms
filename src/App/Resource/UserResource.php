<?php

namespace App\Resource;

use App\AbstractResource;
use App\Entity\User;
use gamringer\JSONPatch\Operation\Exception;
use TypeError;

/**
 * Class Resource
 * @package App
 */
class UserResource extends AbstractResource
{
    /**
     * @param string|null $id
     *
     * @return array
     */
    public function get($id = null)
    {
        if ($id === null) {
            $users = $this->entityManager->getRepository('App\Entity\User')->findAll();
            $users = array_map(
                function (User $user) {
                    return $user->getArrayCopy();
                },
                $users
            );

            return $users;
        } else {
            $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
                array('id' => $id)
            );
            if ($user) {
                return $user->getArrayCopy();
            }
        }

        return null;
    }

    public function post($data)
    {
        $user = new User();
        foreach ($data as $key => $value) {
            $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
            $user->$method($value);
        }
        $user->setApiKey("Basic ".base64_encode($user->getEmail().":".$user->getPassword()));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function patch($id, $data)
    {
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
            array('id' => $id)
        );
        if ($user != null) {
            foreach ($data as $key => $value) {
                $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
                $user->$method($value);
            }
            $user->setApiKey("Basic ".base64_encode($user->getEmail().":".$user->getPassword()));

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $user;
    }

    public function delete($id)
    {
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
            array('id' => $id)
        );
        if ($user != null) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
        }

        return $user;
    }
}