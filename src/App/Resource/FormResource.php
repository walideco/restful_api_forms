<?php

namespace App\Resource;

use App\AbstractResource;
use App\Entity\Form;
use DateTime;

class FormResource extends AbstractResource
{
    public function get($id = null)
    {
        if ($id === null) {
            $forms = $this->entityManager->getRepository('App\Entity\Form')->findAll();
            $forms = array_map(
                function (Form $form) {
                    return $form->getArrayCopy();
                },
                $forms
            );

            return $forms;
        } else {
            $form = $this->entityManager->getRepository('App\Entity\Form')->findOneBy(
                array('id' => $id)
            );
            if ($form) {
                return $form->getArrayCopy();
            }
        }

        return null;
    }

    public function post($data)
    {
        /* Tester si tous les champs sont respectés */

        $form = new Form();
        foreach ($data as $key => $value) {
            $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
                $form->$method($value);
        }

        $now = new DateTime("now");
        $form->setCreationDate($now);
        $form->setUpdateDate($now);

        $this->entityManager->persist($form);
        $this->entityManager->flush();

        return $form;
    }

    public function patch($id, $data)
    {
        $form = $this->entityManager->getRepository('App\Entity\Form')->findOneBy(
            array('id' => $id)
        );
        if ($form != null) {
            foreach ($data as $key => $value) {
                $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
                $form->$method($value);
            }
            $now = new DateTime("now");
            $form->setUpdateDate($now);

            $this->entityManager->persist($form);
            $this->entityManager->flush();
        }

        return $form;
    }

    public function delete($id)
    {
        $form = $this->entityManager->getRepository('App\Entity\Form')->findOneBy(
            array('id' => $id)
        );
        if ($form != null) {
            $this->entityManager->remove($form);
            $this->entityManager->flush();
        }

        return $form;
    }
}