<?php

namespace App\Resource;


use App\AbstractResource;
use App\Entity\Field;

class FieldResource extends AbstractResource
{
    public function get($formId, $fieldId = null)
    {
        if ($fieldId === null) {
            $fields = $this->entityManager->getRepository('App\Entity\Field')->findBy(['formID' => $formId]);
            $fields = array_map(
                function (Field $field) {
                    return $field->getArrayCopy();
                },
                $fields
            );

            return $fields;
        } else {
            $field = $this->entityManager->getRepository('App\Entity\Field')->findOneBy(
                ['formID' => $formId, 'id' => $fieldId]
            );
            if ($field) {
                return $field->getArrayCopy();
            }
        }

        return null;
    }

    public function post($formId, $data)
    {
        $field = new Field();

        foreach ($data as $key => $value) {
            $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
            $field->$method($value);
        }
        $field->setFormID($formId);
        $this->entityManager->persist($field);
        $this->entityManager->flush();

        return $field;
    }

    public function patch($formId, $fieldId, $data)
    {
        $field = $this->entityManager->getRepository('App\Entity\Field')->findOneBy(
            ['formID' => $formId, 'id' => $fieldId]
        );
        if ($field != null) {
            foreach ($data as $key => $value) {
                $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
                $field->$method($value);
            }
            $this->entityManager->persist($field);
            $this->entityManager->flush();
        }

        return $field;
    }

    public function delete($formId, $fieldId)
    {
        $field = $this->entityManager->getRepository('App\Entity\Field')->findOneBy(
            ['formID' => $formId, 'id' => $fieldId]
        );
        if ($field != null) {
            $this->entityManager->remove($field);
            $this->entityManager->flush();
        }

        return $field;
    }

}