<?php
/**
 * Created by IntelliJ IDEA.
 * User: walid
 * Date: 15/10/16
 * Time: 15:03
 */

namespace App\Resource;


use App\AbstractResource;
use App\Entity\Saving;
use DateTime;

class SavingResource extends AbstractResource
{
    public function get($formId, $userId, $savingId = null)
    {
        if ($savingId === null) {
            if ($userId == -1) {
                $savings = $this->entityManager->getRepository('App\Entity\Saving')->findBy(['formID' => $formId]);
            } else {
                $savings = $this->entityManager->getRepository(
                    'App\Entity\Saving')->findBy(['formID' => $formId, "userId" => $userId]);
            }
            $savings = array_map(
                function (Saving $saving) {
                    return $saving->getArrayCopy();
                },
                $savings
            );

            return $savings;
        } else {
            if ($userId == -1) {
                $field = $this->entityManager->getRepository('App\Entity\Saving')->findOneBy(
                    ['formID' => $formId, 'id' => $savingId]
                );
            } else {
                $field = $this->entityManager->getRepository('App\Entity\Saving')->findOneBy(
                    ['formID' => $formId, 'id' => $savingId, "userId" => $userId]
                );
            }
            if ($field) {
                return $field->getArrayCopy();
            }
        }

        return null;
    }

    public
    function post(
        $formId,
        $data
    ) {


        $saving = new Saving();

        foreach ($data as $key => $value) {
            $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
            $saving->$method($value);
        }

        $saving->setFormID($formId);
        $saving->setStatus(0);
        $now = new DateTime("now");
        $saving->setCreationDate($now);
        $saving->setUpdateDate($now);

        $this->entityManager->persist($saving);
        $this->entityManager->flush();

        return $saving;
    }

    public
    function patch(
        $formId,
        $savingId,
        $data
    ) {
        $saving = $this->entityManager->getRepository('App\Entity\Saving')->findOneBy(
            ['formID' => $formId, 'id' => $savingId]
        );
        if ($saving != null) {
            foreach ($data as $key => $value) {
                $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
                $saving->$method($value);
            }
            $now = new DateTime("now");
            $saving->setUpdateDate($now);

            $this->entityManager->persist($saving);
            $this->entityManager->flush();
        }

        return $saving;
    }

    public
    function validate(
        $formId,
        $savingId
    ) {
        $saving = $this->entityManager->getRepository('App\Entity\Saving')->findOneBy(
            ['formID' => $formId, 'id' => $savingId]
        );
        if ($saving != null) {

            $saving->setStatus(1);

            $now = new DateTime("now");
            $saving->setUpdateDate($now);

            $this->entityManager->persist($saving);
            $this->entityManager->flush();
        }

        return $saving;
    }

    public
    function delete(
        $formId,
        $savingId
    ) {
        $saving = $this->entityManager->getRepository('App\Entity\Saving')->findOneBy(
            ['formID' => $formId, 'id' => $savingId]
        );
        if ($saving != null) {
            $this->entityManager->remove($saving);
            $this->entityManager->flush();
        }

        return $saving;
    }
}