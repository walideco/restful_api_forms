<?php
/**
 * Created by IntelliJ IDEA.
 * User: walid
 * Date: 09/10/16
 * Time: 00:40
 */

namespace App\Resource;


use App\AbstractResource;
use App\Entity\FieldType;

class FieldTypeResource extends AbstractResource
{
    public function get($id = null)
    {
        if ($id === null) {
            $types = $this->entityManager->getRepository('App\Entity\Form')->findAll();
            $types = array_map(
                function (FieldType $type) {
                    return $type->getArrayCopy();
                },
                $types
            );

            return $types;
        } else {
            $type = $this->entityManager->getRepository('App\Entity\FieldType')->findOneBy(
                array('id' => $id)
            );
            if ($type) {
                return $type->getArrayCopy();
            }
        }

        return null;
    }

    public function post($data)
    {
        /* Tester si tous les champs sont respectés */

        $type = new FieldType();
        foreach ($data as $key => $value) {
            $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
            $type->$method($value);
        }
        $this->entityManager->persist($type);
        $this->entityManager->flush();

        return $type;
    }

    public function patch($id, $data)
    {
        $type = $this->entityManager->getRepository('App\Entity\FieldType')->findOneBy(
            array('id' => $id)
        );
        if ($type != null) {
            foreach ($data as $key => $value) {
                $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
                $type->$method($value);
            }
            $this->entityManager->persist($type);
            $this->entityManager->flush();
        }

        return $type;
    }

    public function delete($id)
    {
        $type = $this->entityManager->getRepository('App\Entity\FieldType')->findOneBy(
            array('id' => $id)
        );
        if ($type != null) {
            $this->entityManager->remove($type);
            $this->entityManager->flush();
        }

        return $type;
    }
}