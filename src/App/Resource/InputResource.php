<?php

namespace App\Resource;

use App\AbstractResource;
use App\Entity\Input;
use Doctrine\DBAL\Exception\InvalidArgumentException;

class InputResource extends AbstractResource
{
    public function get($formId, $savingId, $inputId = null)
    {
        if ($inputId === null) {
            $inputs = $this->entityManager->getRepository('App\Entity\Input')
                ->findBy(['formID' => $formId, 'savingID' => $savingId]);
            $inputs = array_map(
                function (Input $input) {
                    return $input->getArrayCopy();
                },
                $inputs
            );

            return $inputs;
        } else {

            $input = $this->entityManager->getRepository('App\Entity\Input')->findOneBy(
                ['formID' => $formId, 'savingID' => $savingId, 'id' => $inputId]
            );
            if ($input) {
                return $input->getArrayCopy();
            }
        }

        return null;
    }

    public function post($formId, $savingId, $data)
    {
        $saving = $this->entityManager->getRepository('App\Entity\Saving')->findOneBy(
            ['formID' => $formId, 'savingID' => $savingId,'status' => 0 ]
        );

        if(!$saving){
            throw new InvalidArgumentException();
        }

        $input = new Input();

        foreach ($data as $key => $value) {
            $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
            $input->$method($value);
        }
        $input->setFormID($formId);
        $input->setSavingID($savingId);
        $this->entityManager->persist($input);
        $this->entityManager->flush();

        return $input;
    }

    public function patch($formId, $savingId, $inputId, $data)
    {
        $saving = $this->entityManager->getRepository('App\Entity\Saving')->findOneBy(
            ['formID' => $formId, 'savingID' => $savingId,'status' => 0 ]
        );

        if(!$saving){
            throw new InvalidArgumentException();
        }

        $input = $this->entityManager->getRepository('App\Entity\Input')->findOneBy(
            ['formID' => $formId, 'savingID' => $savingId, 'id' => $inputId]
        );
        if ($input != null) {
            foreach ($data as $key => $value) {
                $method = 'set'.strtoupper(substr($key, 0, 1)).substr($key, 1, strlen($key) - 1);
                $input->$method($value);
            }
            $this->entityManager->persist($input);
            $this->entityManager->flush();
        }

        return $input;
    }

    public function delete($formId, $savingId, $inputId)
    {
        $input = $this->entityManager->getRepository('App\Entity\Input')->findOneBy(
            ['formID' => $formId, 'savingID' => $savingId, 'id' => $inputId]
        );
        if ($input != null) {
            $this->entityManager->remove($input);
            $this->entityManager->flush();
        }

        return $input;
    }

}