<?php

namespace App\Entity;


use App\AbstractEntity;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="savings")
 */
class Saving extends AbstractEntity
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var DateTime
     * @ORM\Column(name="creation_date",type="datetime")
     */
    protected $creationDate;

    /**
     * @var DateTime
     * @ORM\Column(name="update_date",type="datetime")
     */
    protected $updateDate;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $formID;


    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    protected $userID;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return DateTime
     */
    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param DateTime $creationDate
     */
    public function setCreationDate(DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return DateTime
     */
    public function getUpdateDate(): DateTime
    {
        return $this->updateDate;
    }

    /**
     * @param DateTime $updateDate
     */
    public function setUpdateDate(DateTime $updateDate)
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }
    /**
     * @return int
     */
    public function getFormID(): int
    {
        return $this->formID;
    }

    /**
     * @param int $formID
     */
    public function setFormID(int $formID)
    {
        $this->formID = $formID;
    }
    /**
     * @return int
     */
    public function getUserID(): int
    {
        return $this->userID;
    }

    /**
     * @param int $userID
     */
    public function setUserID(int $userID)
    {
        $this->userID = $userID;
    }




}