<?php
namespace App\Entity;

use App\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="inputs")
 *
 */
class Input extends AbstractEntity
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     *
     */
    protected $formID;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     *
     */
    protected $fieldID;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     *
     */
    protected $savingID;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     *
     *
     */
    protected $value;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getFieldID(): int
    {
        return $this->fieldID;
    }

    /**
     * @param int $fieldID
     */
    public function setFieldID(int $fieldID)
    {
        $this->fieldID = $fieldID;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getFormID(): int
    {
        return $this->formID;
    }

    /**
     * @param int $formID
     */
    public function setFormID(int $formID)
    {
        $this->formID = $formID;
    }

    /**
     * @return int
     */
    public function getSavingID(): int
    {
        return $this->savingID;
    }

    /**
     * @param int $savingID
     */
    public function setSavingID(int $savingID)
    {
        $this->savingID = $savingID;
    }


}