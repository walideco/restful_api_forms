<?php

namespace App;

class AbstractEntity
{
    /**
     * Get array copy of object
     *
     * @return array
     */

    //TODO : cascade foreign keys / Form <- Field  / ( Form ,Field , NumSaisie) <- Input / FieldType <- Field
    //TODO meta data ( a verifier)
    // TODO location avec non du path apres un post
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}