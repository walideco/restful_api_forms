<?php

namespace App\Controller;


use App\Resource\FormResource;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Error;
use Slim\Http\Request;
use Slim\Http\Response;
use TypeError;

class FormController
{
    private $formResource;

    public function __construct(FormResource $formResource)
    {
        $this->formResource = $formResource;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms",
     *     summary="Get all forms",
     *     description="Returns a list of form's model",
     *     operationId="getUForms",
     *     tags={"form"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/Form")
     *         ),
     *     )
     * )
     */

    public function getForms(Request $request, Response $response, $args)
    {
        $forms = $this->formResource->get();

        return $response->withJSON(['data' => $forms]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms/{formId}",
     *     summary="Get one Form",
     *     description="Returns a single Form",
     *     operationId="getForm",
     *     tags={"form"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form to return",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/form")
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Form not found"
     *     )
     * )
     */
    public function getForm(Request $request, Response $response, $args)
    {
        $form = $this->formResource->get($args['formId']);
        if ($form) {
            return $response->withJSON(['data' => $form]);
        }

        return $response->withStatus(404, 'No Form found with id '.$args['formId']);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Post(
     *     path="/forms",
     *     tags={"form"},
     *     operationId="addForm",
     *     summary="Create a new Form model",
     *     description="",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     )
     * )
     */

    public function create(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $form = $this->formResource->post($data);
            if ($form) {
                return $response->withJson(["status" => "ok"], 201)
                    ->withAddedHeader('Location', ['id' => $form->getId()]);
            }


        } catch (NotNullConstraintViolationException $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Required propriety missing"]], 400);
        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ["message" => "Insertion error"]], 500);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Patch(
     *     path="/forms/{formId}",
     *     tags={"form"},
     *     operationId="updateForm",
     *     summary="Update Form",
     *     description="update one form model",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model to update",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *    @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Form does not exist" ,
     *     )
     * )
     */
    public function update(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $form = $this->formResource->patch($args['formId'], $data);
            if ($form) {
                return $response->withJson(["status" => "ok"]);
            }
        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        }

        return $response->withJson(["error" => ['message' => 'Form '.$args['formId'].' does not exist']], 404);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Delete(
     *     path="/forms/{formId}",
     *     tags={"form"},
     *     operationId="deleteForm",
     *     summary="Delete Form",
     *     description="Delete one Form model by ID",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form to update",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Form does not exist" ,
     *     )
     * )
     */
    public function delete(Request $request, Response $response, $args)
    {
        $form = $this->formResource->delete($args['formId']);
        if ($form) {
            return $response->withJson(["status" => "ok"]);
        }

        return $response->withJson(["error" => ['message' => 'Form '.$args['formId'].' does not exist']], 404);
    }
}