<?php

namespace App\Controller;

use App\Resource\UserResource;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Error;
use Slim\Http\Request;
use Slim\Http\Response;
use TypeError;

final class UserController
{
    private $userResource;

    public function __construct(UserResource $userResource)
    {
        $this->userResource = $userResource;
    }

    /**
     * /**
     * @SWG\Get(
     *     path="/users",
     *     summary="Get all user",
     *     description="Returns the list of users",
     *     operationId="getUsers",
     *     tags={"user"},
     *     consumes={
     *         "application/json"
     *     },
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/User")
     *         ),
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Users not found"
     *     )
     * )
     */

    public function getUsers(Request $request, Response $response, $args)
    {
        $users = $this->userResource->get();

        return $response->withJSON(['data' => $users]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/users/{userId}",
     *     summary="Get one user",
     *     description="Returns a single user",
     *     operationId="getUser",
     *     tags={"user"},
     *     consumes={
     *         "application/json",
     *     },
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of user to return",
     *         in="path",
     *         name="userId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/user")
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="User not found"
     *     )
     * )
     */


    public function getUser(Request $request, Response $response, $args)
    {
        $user = $this->userResource->get($args['userId']);
        if ($user) {
            return $response->withJSON(['data' => $user->getArrayCopy()]);
        }

        return $response->withStatus(404, 'No User found with id '.$args['userId']);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Post(
     *     path="/users",
     *     tags={"user"},
     *     operationId="createUser",
     *     summary="Create User",
     *     description="Create a new User",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="fistName",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lastName",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         required=true,
     *         enum={"0","1"}
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     )
     * )
     */
    public function create(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $user = $this->userResource->post($data);
            if ($user) {
                return $response->withJson(["status" => "ok"], 201)
                    ->withAddedHeader('Location', ['id' => $user->getId()]);
            }
        } catch (NotNullConstraintViolationException $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: User's propriety missing"]], 400);

        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ["message" => "Insertion error"]], 500);

    }
    /**
     * @SWG\Patch(
     *     path="/users/{userId}",
     *     tags={"user"},
     *     operationId="updateUser",
     *     summary="Update User",
     *     description="Update one User",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of user to update",
     *         in="path",
     *         name="userId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="fistName",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="lastName",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="body",
     *         required=false,
     *         enum={"0","1"}
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="User does not exist" ,
     *     )
     * )
     */
    public function update(Request $request, Response $response, $args)
    {

        $data = $request->getParsedBody();
        try {
            $user = $this->userResource->patch($args['userId'], $data);
            if ($user) {
                return $response->withJson(["status" => "ok"]);
            }
        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Type error "]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ['message' => 'User '.$args['userId'].' does not exist']], 404);
    }

    /**
     *
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Delete(
     *     path="/users/{userId}",
     *     tags={"user"},
     *     operationId="deleteUser",
     *     summary="Delete User",
     *     description="Delete one user",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of user to update",
     *         in="path",
     *         name="userId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="User does not exist" ,
     *     )
     * )
     */
    public function delete(Request $request, Response $response, $args)
    {
        if (!$args['userId']) {
            return $response->withJson(["error" => ["message" => "Missing 'id' "]], 400);
        }
        $user = $this->userResource->delete($args['userId']);
        if ($user) {
            return $response->withJson(["status" => "ok"]);
        }

        return $response->withJson(["error" => ['message' => 'User '.$args['userId'].' does not exist']], 404);
    }
}