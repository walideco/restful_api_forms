<?php
/**
 * Created by IntelliJ IDEA.
 * User: walid
 * Date: 09/10/16
 * Time: 00:39
 */

namespace App\Controller;


use App\Resource\FieldTypeResource;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Error;
use Slim\Http\Request;
use Slim\Http\Response;
use TypeError;

class FieldTypeController
{
    private $typeFieldResource;

    public function __construct(FieldTypeResource $fieldTypeResource)
    {
        $this->typeFieldResource = $fieldTypeResource;
    }

    public function getTypes(Request $request, Response $response, $args)
    {
        $forms = $this->typeFieldResource->get();

        return $response->withJSON(['data' => $forms]);
    }

    public function getType(Request $request, Response $response, $args)
    {
        $form = $this->typeFieldResource->get($args['formId']);
        if ($form) {
            return $response->withJSON(['data' => $form]);
        }

        return $response->withStatus(404, 'No Form found with id '.$args['formId']);
    }

    public function create(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $form = $this->typeFieldResource->post($data);
            if ($form) {
                return $response->withJson(["status" => "ok"], 201)
                    ->withAddedHeader('Location', ['id' => $form->getId()]);
            }

        } catch (NotNullConstraintViolationException $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Required propriety missing"]], 400);

        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        }catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ["message" => "Insertion error"]], 500);
    }

    public function update(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $form = $this->typeFieldResource->patch($args['formId'], $data);
            if ($form) {
                return $response->withJson(["status" => "ok"]);
            }
        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        }catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ['message' => 'Form '.$args['formId'].' does not exist']], 404);
    }

    public function delete(Request $request, Response $response, $args)
    {
        $form = $this->typeFieldResource->delete($args['formId']);
        if ($form) {
            return $response->withJson(["status" => "ok"]);
        }

        return $response->withJson(["error" => ['message' => 'Form '.$args['formId'].' does not exist']], 404);
    }
}