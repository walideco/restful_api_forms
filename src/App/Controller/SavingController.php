<?php

namespace App\Controller;


use App\Resource\SavingResource;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Error;
use Slim\Http\Request;
use Slim\Http\Response;
use TypeError;


class SavingController
{
    private $savingResource;

    public function __construct(SavingResource $savingResource)
    {
        $this->savingResource = $savingResource;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms/{formId}/savings",
     *     summary="Get all savings",
     *     description="Returns a list of savings for the specified form's model",
     *     operationId="getSavings",
     *     tags={"saving"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *         description="ID of savings form",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/Saving")
     *         ),
     *     )
     * )
     */


    public function getSavings(Request $request, Response $response, $args)
    {
        $user = $request->getAttribute("userId");


        $savings = $this->savingResource->get($args['formId']);

        return $response->withJSON(['data' => $savings]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms/{formId}/savings/{savingId}",
     *     summary="Get one Saving",
     *     description="Returns a single Saving",
     *     operationId="getSaving",
     *     tags={"saving"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of the saving",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving to return",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/saving")
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Saving not found"
     *     )
     * )
     */

    public function getSaving(Request $request, Response $response, $args)
    {
        $saving = $this->savingResource->get($args['formId'], $args['savingId']);
        if ($saving) {
            return $response->withJSON(['data' => $saving]);
        }

        return $response->withStatus(404, 'No User found with id'.$args['savingId']);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Post(
     *     path="/forms/{formId}/savings",
     *     tags={"saving"},
     *     operationId="addSaving",
     *     summary="Create a new Saving",
     *     description="Create a new Saving from the specified form",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="formData",
     *         required=true,
     *         type ="integer"
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     )
     * )
     */

    public function create(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $saving = $this->savingResource->post($args['formId'], $data);
            if ($saving) {
                return $response->withJson(["status" => "ok"], 201)
                    ->withAddedHeader('Location', ['id' => $saving->getId()]);
            }
        } catch (NotNullConstraintViolationException $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Required propriety is missing"]], 400);

        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ["message" => "Insertion error:"]], 500);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Patch(
     *     path="/forms/{formId}/savings/{savingId}",
     *     tags={"saving"},
     *     operationId="updateSaving",
     *     summary="Update Saving",
     *     description="update one Saving",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of saving to update",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving to update",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="userId",
     *         in="body",
     *         required=false,
     *         type ="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Form does not exist" ,
     *     )
     * )
     */
    public function update(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $saving = $this->savingResource->patch($args['formId'], $args['savingId'], $data);
            if ($saving) {
                return $response->withJson(["status" => "ok"]);
            }
        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type"]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ['message' => 'Saving '.$args['savingId'].' does not exist']], 404);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Patch(
     *     path="/forms/{formId}/savings/{savingId}/valid",
     *     tags={"saving"},
     *     operationId="validSaving",
     *     summary="Validate Saving",
     *     description="Validate one Saving",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of saving to update",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving to update",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Form does not exist" ,
     *     )
     * )
     */

    public function validate(Request $request, Response $response, $args)
    {
        $saving = $this->savingResource->validate($args['formId'], $args['savingId']);
        if ($saving) {
            return $response->withJson(["status" => "ok"]);
        }

        return $response->withJson(["error" => ['message' => 'Saving '.$args['savingId'].' does not exist']], 404);
    }
    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Delete(
     *     path="/forms/{formId}/savings/{savingId}",
     *     tags={"saving"},
     *     operationId="deleteSaving",
     *     summary="Delete Saving",
     *     description="Delete one Saving",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of saving",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving to delete",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Form does not exist" ,
     *     )
     * )
     */
    public function delete(Request $request, Response $response, $args)
    {
        $saving = $this->savingResource->delete($args['formId'], $args['savingId']);
        if ($saving) {
            return $response->withJson(["status" => "ok"]);
        }

        return $response->withJson(["error" => ['message' => 'Saving '.$args['savingId'].' does not exist']], 404);
    }

}