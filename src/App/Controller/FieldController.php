<?php

namespace App\Controller;


use App\Resource\FieldResource;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Error;
use Slim\Http\Request;
use Slim\Http\Response;
use TypeError;

class FieldController
{
    private $fieldResource;

    public function __construct(FieldResource $fieldResource)
    {
        $this->fieldResource = $fieldResource;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms/{formId}/fields",
     *     summary="Get all fields",
     *     description="Returns a list of fields for the specified form's model",
     *     operationId="getFields",
     *     tags={"field"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *         description="ID of form",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/field")
     *         ),
     *     )
     * )
     */

    public function getFields(Request $request, Response $response, $args)
    {
        $fields = $this->fieldResource->get($args['formId']);

        return $response->withJSON(['data' => $fields]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms/{formId}/fields/{fieldId}",
     *     summary="Get one Field",
     *     description="Returns a single Field",
     *     operationId="getField",
     *     tags={"field"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of field to return",
     *         in="path",
     *         name="fieldId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/field")
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Saving not found"
     *     )
     * )
     */


    public function getField(Request $request, Response $response, $args)
    {
        $field = $this->fieldResource->get($args['formId'], $args['fieldId']);
        if ($field) {
            return $response->withJSON(['data' => $field]);
        }

        return $response->withStatus(404, 'No User found with id'.$args['fieldId']);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Post(
     *     path="/forms/{formId}/fields",
     *     tags={"field"},
     *     operationId="addField",
     *     summary="Create a new Field",
     *     description="Create a new Field for the specified form",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="typeId",
     *         in="formData",
     *         required=true,
     *         type ="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     )
     * )
     */

    public function create(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $field = $this->fieldResource->post($args['formId'], $data);
            if ($field) {
                return $response->withJson(["status" => "ok"], 201)
                    ->withAddedHeader('Location', ['id' => $field->getId()]);
            }
        } catch (NotNullConstraintViolationException $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Required propriety is missing"]], 400);

        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ["message" => "Insertion error:"]], 500);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Patch(
     *     path="/forms/{formId}/fields/{fieldId}",
     *     tags={"field"},
     *     operationId="updateField",
     *     summary="Update Field",
     *     description="update one Field",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of field to update",
     *         in="path",
     *         name="fieldId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="fieldId",
     *         in="body",
     *         required=false,
     *         type ="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="description",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Field does not exist" ,
     *     )
     * )
     */


    public function update(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $field = $this->fieldResource->patch($args['formId'], $args['fieldId'], $data);
            if ($field) {
                return $response->withJson(["status" => "ok"]);
            }
        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type"]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ['message' => 'Field '.$args['fieldId'].' does not exist']], 404);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\delete(
     *     path="/forms/{formId}/fields/{fieldId}",
     *     tags={"field"},
     *     operationId="deleteField",
     *     summary="Delete Field",
     *     description="Delete one Field",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of saving",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of field to delete",
     *         in="path",
     *         name="fieldId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Field does not exist" ,
     *     )
     * )
     */

    public function delete(Request $request, Response $response, $args)
    {
        $form = $this->fieldResource->delete($args['formId'], $args['fieldId']);
        if ($form) {
            return $response->withJson(["status" => "ok"]);
        }

        return $response->withJson(["error" => ['message' => 'Field '.$args['fieldId'].' does not exist']], 404);
    }

}