<?php

namespace App\Controller;


use App\Resource\InputResource;
use Doctrine\DBAL\Exception\InvalidArgumentException;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Error;
use Slim\Http\Request;
use Slim\Http\Response;
use TypeError;

class InputController
{
    private $inputResource;

    public function __construct(InputResource $inputResource)
    {
        $this->inputResource = $inputResource;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms/{formId}/savings/{savingId}/inputs",
     *     summary="Get all inputs",
     *     description="Returns a list of inputs for the specified Saving",
     *     operationId="getInputs",
     *     tags={"input"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of the saving",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving to return",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(ref="#/definitions/Inputs")
     *         ),
     *     )
     * )
     */

    public function getInputs(Request $request, Response $response, $args)
    {
        $inputs = $this->inputResource->get($args['formId'], $args['savingId']);

        return $response->withJSON(['data' => $inputs]);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Get(
     *     path="/forms/{formId}/savings/{savingId}/inputs/{inputId}",
     *     summary="Get one input",
     *     description="Returns a single Input",
     *     operationId="getSaving",
     *     tags={"input"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of the input",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of input to return",
     *         in="path",
     *         name="inputId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/input")
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Input not found"
     *     )
     * )
     */


    public function getInput(Request $request, Response $response, $args)
    {
        $input = $this->inputResource->get($args['formId'], $args['savingId'], $args['inputId']);
        if ($input) {
            return $response->withJSON(['data' => $input]);
        }

        return $response->withStatus(404, 'No User found with id'.$args['inputId']);
    }


    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Post(
     *     path="/forms/{formId}/savings/{savingId}/inputs",
     *     tags={"input"},
     *     operationId="addInput",
     *     summary="Create a new Input",
     *     description="Create a new Input the specified saving",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of the input",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="value",
     *         in="formData",
     *         required=true,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="fieldId",
     *         in="formData",
     *         required=true,
     *         type ="integer"
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     )
     * )
     */


    public function create(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $input = $this->inputResource->post($args['formId'], $args['savingId'], $data);
            if ($input) {
                return $response->withJson(["status" => "ok"], 201)
                    ->withAddedHeader('Location', ['id' => $input->getId()]);
            }
        } catch (InvalidArgumentException $e) {
            return $response->withJson(["error" => ["message" => "Precondition Failed: Saving already validated"]],
                412);
        } catch (NotNullConstraintViolationException $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Required propriety is missing"]], 400);

        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type "]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ["message" => "Insertion error:"]], 500);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Patch(
     *     path="/forms/{formId}/savings/{savingId}/inputs/{inputId}",
     *     tags={"input"},
     *     operationId="updateInput",
     *     summary="Update Input",
     *     description="update one Input",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of the input",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of input to update",
     *         in="path",
     *         name="inputId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         name="value",
     *         in="body",
     *         required=false,
     *         type ="string"
     *     ),
     *     @SWG\Parameter(
     *         name="fieldId",
     *         in="body",
     *         required=false,
     *         type ="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Bad Request",
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Input does not exist" ,
     *     )
     * )
     */
    public function update(Request $request, Response $response, $args)
    {
        $data = $request->getParsedBody();
        try {
            $input = $this->inputResource->patch($args['formId'], $args['savingId'], $args['inputId'], $data);
            if ($input) {
                return $response->withJson(["status" => "ok"]);
            }
        } catch (InvalidArgumentException $e) {
            return $response->withJson(["error" => ["message" => "Precondition Failed: saving already validated"]],
                412);
        } catch (TypeError $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Error type"]], 400);
        } catch (Error $e) {
            return $response->withJson(["error" => ["message" => "Bad Request: Parameters name error"]], 400);
        }

        return $response->withJson(["error" => ['message' => 'Field '.$args['inputId'].' does not exist']], 404);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     *
     * @SWG\Delete(
     *     path="/forms/{formId}/savings/{savingId}/inputs/{inputId}",
     *     tags={"input"},
     *     operationId="deleteInput",
     *     summary="Delete Input",
     *     description="Delete one Input",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of form model of the input",
     *         in="path",
     *         name="formId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of saving",
     *         in="path",
     *         name="savingId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Parameter(
     *         description="ID of input to delete",
     *         in="path",
     *         name="inputId",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     )
     *     @SWG\Response(
     *         response=404,
     *         description="Input does not exist" ,
     *     )
     * )
     */

    public function delete(Request $request, Response $response, $args)
    {
        $form = $this->inputResource->delete($args['formId'], $args['savingId'], $args['inputId']);
        if ($form) {
            return $response->withJson(["status" => "ok"]);
        }

        return $response->withJson(["error" => ['message' => 'User '.$args['inputId'].' does not exist']], 404);
    }

}