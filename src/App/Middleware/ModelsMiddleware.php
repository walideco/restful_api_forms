<?php

namespace App\Middleware;

use App\AbstractMiddleware;
use Slim\Http\Request;
use Slim\Http\Response;

class ModelsMiddleware extends AbstractMiddleware
{

    public function __invoke(Request $request, Response $response, $next)
    {
        $apiKey = $request->getHeader('Authorization');
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
            ['apiKey' => $apiKey]
        );
        if ($request->getMethod() != "GET" && $user->getGrade() == 0) {
            return $response->withJson([
                "error" => [
                    "message"  => "access denied",
                ],
            ], 403);
        }
        $response = $next($request, $response);

        return $response;
    }
}