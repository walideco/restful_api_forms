<?php
/**
 * Created by IntelliJ IDEA.
 * User: walid
 * Date: 23/10/16
 * Time: 19:01
 */

namespace App\Middleware;


use App\AbstractMiddleware;
use Slim\Http\Request;
use Slim\Http\Response;

class SavingsMiddleware extends AbstractMiddleware
{

    public function __invoke(Request $request, Response $response, $next)
    {
        $apiKey = $request->getHeader('Authorization');
        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(
            ['apiKey' => $apiKey]
        );
        $body =  $request->getParsedBody();

        if($request->getMethod() == "POST" && $user->getGrade() == 0 && $body["userId"] != $user->getId()){
            return $response->withJson([
                "error" => [
                    "message"  => "access denied",
                ],
            ], 403);
        }
        if($request->getMethod() == "PATCH" && $user->getGrade() == 0 ){
            $array= explode("/",$request->getRequestTarget());
            if($array[count($array-1)]!= "valid"){
                return $response->withJson([
                    "error" => [
                        "message"  => "access denied",
                    ],
                ], 403);
            }
}
        if ($request->getMethod() == "GET" && $user->getGrade() == 0) {
            $request->withAttribute("userId", $user["id"]);
        }
        $response = $next($request, $response);

        return $response;
    }
}