<?php
/**
 * Created by IntelliJ IDEA.
 * User: walid
 * Date: 23/10/16
 * Time: 20:12
 */

namespace App\Middleware;


use App\AbstractMiddleware;
use Slim\Http\Request;
use Slim\Http\Response;

class InputsMiddleware extends AbstractMiddleware
{

    public function __invoke(Request $request, Response $response, $next)
    {

        $response = $next($request, $response);

        return $response;
    }
}