<?php

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="localhost",
 *     basePath="/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Form Model Manager",
 *         @SWG\Contact(
 *             email="ettaieb.walid@gmail.com"
 *         ),
 *         @SWG\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *         )
 *     )
 * )
 */
