<?php
// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);

use Slim\Http\Request;
use Slim\Http\Response;

$container = $app->getContainer();

$authenticator = function (Request $request, Response $response, $next) {
    global $container;
    $em = $container['em'];
    $apiKey = $request->getHeader('Authorization');
    $user = $em->getRepository('App\Entity\User')->findOneBy(
        ['apiKey' => $apiKey]
    );
    if (!$user) {
        return $response->withJson([
            "error" => [
                "message" => "invalid_grant",
                "error_description" => "Invalid username and password combination",
            ],
        ], 401);
    }

    $response = $next($request, $response);
    return $response;
};

$app->add($authenticator);



//$rights = [""]